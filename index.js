const express = require("express");
const mongoose = require("mongoose");
// import routers
const { customerRouter } = require("./app/routers/customer.router");
const { orderRouter } = require("./app/routers/order.router");
const { orderDetailRouter } = require("./app/routers/orderDetail.router");
const { productRouter } = require("./app/routers/product.router");
const { productTypeRouter } = require("./app/routers/productType.router");
const app = express();

// kết nối với MongoDB
mongoose.set('strictQuery', true);
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Shop24h", err => {
    if (err) throw err;
    console.log("Connect to MongoDB successful");
});

// Thiết lập tiêu đề CORS
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

// config đọc được body json
app.use(express.json());

app.get("/", (req, res) => {
    let time = new Date().toLocaleString();
    console.log("Current time:", time);
    res.json({
        message: "Devcamp Shop24h",
        time
    })
})

// chạy router Product Type
app.use("/", productTypeRouter);
// chạy router Product
app.use("/", productRouter);
// chạy router Customer
app.use("/", customerRouter);
// chạy router Order
app.use("/", orderRouter);
// chạy router orderDetail
app.use("/", orderDetailRouter);


// config để app chạy trên port 8000:
const port = 8000;
app.listen(port, () => {
    console.log("App listening on port:", port);
})