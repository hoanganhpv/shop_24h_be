const express = require("express");
const { createCustomer, getAllCustomers, getCustomerDetailById, updateCustomer, deleteCustomer } = require("../controllers/customer.controller");
const customerRouter = express.Router();
const { customerMiddleware } = require("../middlewares/customer.middleware");

// router create product
customerRouter.post("/customers", customerMiddleware, createCustomer);
// router get all products: 
customerRouter.get("/customers", customerMiddleware, getAllCustomers);
// router get product detail by ID
customerRouter.get("/customers/:customerId", customerMiddleware, getCustomerDetailById);
// router update product by ID
customerRouter.put("/customers/:customerId", customerMiddleware, updateCustomer);
// router update product by ID
customerRouter.delete("/customers/:customerId", customerMiddleware, deleteCustomer);

module.exports = { customerRouter }