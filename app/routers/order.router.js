const express = require("express");
const { createOrderOfCustomer, getAllOrdersDatabase, getAllOrdersOfCustomer, getOrderDetailById, updateOrder, deleteOrder } = require("../controllers/order.controller");
const { orderMiddleware } = require("../middlewares/order.middleware");

const orderRouter = express.Router();

// router create order of customer
orderRouter.post("/customers/:customerId/orders", orderMiddleware, createOrderOfCustomer);

// router get all orders in database: 
orderRouter.get("/orders", orderMiddleware, getAllOrdersDatabase);

// router get all orders of one customer: 
orderRouter.get("/customers/:customerId/orders", orderMiddleware, getAllOrdersOfCustomer);

// router get order detail by ID
orderRouter.get("/orders/:orderId", orderMiddleware, getOrderDetailById);

// router update order by ID
orderRouter.put("/orders/:orderId", orderMiddleware, updateOrder);

// router delete order by ID
orderRouter.delete("/customers/:customerId/orders/:orderId", orderMiddleware, deleteOrder);

module.exports = {orderRouter};