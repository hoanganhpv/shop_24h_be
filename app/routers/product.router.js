const express = require("express");
const { createProduct, getAllProducts, getProductById, updateProduct, deleteProduct, getProductByTypeId, getProductByFilterConditon } = require("../controllers/product.controller");
// import middlewares
const { productMiddleware, getProductsByNoPagesAndTypeMiddleware } = require("../middlewares/product.middleware");
const productRouter = express.Router();

// router create product
productRouter.post("/products", productMiddleware, createProduct);
// router get all products: 
productRouter.get("/products", productMiddleware, getAllProducts);
// router get product detail by ID
productRouter.get("/products/:productId", productMiddleware, getProductById);

productRouter.get("/products/type/:typeId", productMiddleware, getProductByTypeId);

// router update product by ID
productRouter.put("/products/:productId", productMiddleware, updateProduct);
// router update product by ID
productRouter.delete("/products/:productId", productMiddleware, deleteProduct);


// get product with filter condition (number of products per page and type product)
productRouter.get("/condition", getProductsByNoPagesAndTypeMiddleware, getProductByFilterConditon);

module.exports = { productRouter }