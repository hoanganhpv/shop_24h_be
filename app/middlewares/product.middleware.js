const productMiddleware = (req, res, next) => {
    console.log("METHOD: " + req.method);
    next();
}

const getProductsByNoPagesAndTypeMiddleware = (req, res, next) => {
    console.log('Get product with filter condition!');
    next();
}

module.exports = { productMiddleware, getProductsByNoPagesAndTypeMiddleware }; 