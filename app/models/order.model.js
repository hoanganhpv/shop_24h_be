const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const orderSchema = new Schema({
    orderDate: {
        type: Date,
        default: Date.now(),
    },
    shippedDate: {
        type: Date
    },
    note: {
        type: String,
    },
    orderDetails: [{
        type: mongoose.Types.ObjectId,
        ref: "order_detail"
    }],
    cost: {
        type: Number,
        default: 0
    }
}, {
    timestamps: true
})

module.exports = mongoose.model("order", orderSchema);