const mongoose = require("mongoose");
const productModel = require("../models/product.model");

// Create product
const createProduct = (req, res) => {
    // B1: Thu thập dữ liệu
    let body = req.body;
    // B2: Kiểm tra dữ liệu
    // check name
    if (body.name === undefined || body.name.trim() === "") {
        return res.status(400).json({
            status: "Bad Request",
            message: "Name is invalid"
        });
    }
    // check description
    if (body.description !== undefined && body.description.trim() === "") {
        return res.status(400).json({
            status: "Bad Request",
            message: "Description is invalid"
        });
    }
    // check type
    if (!mongoose.Types.ObjectId.isValid(body.type)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Product-Type is invalid"
        });
    }
    // check imageUrl
    if (body.imageUrl === undefined || body.imageUrl.trim() === "") {
        return res.status(400).json({
            status: "Bad Request",
            message: "Image URL is invalid"
        });
    }
    // check buyPrice
    if (!Number.isInteger(body.buyPrice) || body.buyPrice < 0) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Buy Price is invalid"
        });
    }
    // check promotionPrice
    if (!Number.isInteger(body.promotionPrice) || body.promotionPrice < 0) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Promotion Price is invalid"
        });
    }
    // check amount
    if (!Number.isInteger(body.amount) || body.amount < 0) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Amount is invalid"
        });
    }

    // B3: Gọi model và xử lý CSDL
    let newProduct = {
        name: body.name,
        description: body.description,
        type: body.type,
        imageUrl: body.imageUrl,
        buyPrice: body.buyPrice,
        promotionPrice: body.promotionPrice,
        amount: body.amount
    }
    productModel.create(newProduct, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Server Internal Error",
                message: err.message
            });
        }
        else {
            return res.status(201).json({
                status: "Create Product successful",
                data
            });
        }
    })
}

// Get all products
const getAllProducts = (req, res) => {
    // B1: Thu thập dữ liệu
    // B2: Kiểm tra dữ liệu
    // B3: Gọi model và xử lý CSDL
    productModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Server Internal Error",
                message: err.message
            });
        }
        else {
            return res.status(200).json({
                status: "Get All Product successful",
                data
            });
        }
    })
}

// Get product detail by ID
const getProductById = (req, res) => {
    // B1: Thu thập dữ liệu
    console.log('get a Product by ID');
    let productId = req.params.productId;
    // B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Product ID is invalid"
        });
    }
    // B3: Gọi model và xử lý CSDL
    productModel.findById(productId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Server Internal Error",
                message: err.message
            });
        }
        else {
            return res.status(200).json({
                status: "Get Product Detail successful",
                data
            });
        }
    })
};

const getProductByTypeId = async (req, res) => {
    try {
        let typeId = req.params.typeId;
        if (!mongoose.Types.ObjectId.isValid(typeId)) {
            return res.status(400).json({
                status: "Bad Request",
                message: "Product ID is invalid"
            });
        };

        const productsResult = await productModel.find({
            type: typeId
        });

        if (!productsResult) {
            return res.status(400).json({
                status: "Null",
                result: []
            });
        } else {
            return res.status(200).json({
                status: "Get Products success!",
                result: productsResult
            });
        };
    } catch (err) {
        console.log(err);
    } finally {
        console.log(`done process!`);
    };
};

const getProductByFilterConditon = async (req, res) => {
    try {
        const { page, limit } = req.query;
        const skip = (page - 1) * limit;
        const totalProducts = await productModel.countDocuments();
        const totalPages = Math.ceil(totalProducts / limit);
        const products = await productModel.find().skip(skip).limit(limit);
        res.json({
            totalPages: totalPages,
            products: products
        });
    } catch (err) {
        console.log(err);
        return res.status(500).json({
            errorMessages: "Internal server error!"
        })
    } finally {
        console.log(`done process get products by filter condition!`);
    };
};

// Update product by ID
const updateProduct = (req, res) => {
    // B1: Thu thập dữ liệu
    let productId = req.params.productId;
    let body = req.body;
    // B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Product-Type is invalid"
        });
    }
    // check if name is exist but not valid
    if (body.name !== undefined && body.name.trim() === "") {
        return res.status(400).json({
            status: "Bad Request",
            message: "Name is invalid"
        });
    }
    // check if description is exist but not valid
    if (body.description !== undefined && body.description.trim() === "") {
        return res.status(400).json({
            status: "Bad Request",
            message: "Description is invalid"
        });
    }
    // check if type is exist but not valid
    if (!mongoose.Types.ObjectId.isValid(body.type)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Product-Type is invalid"
        });
    }
    // check if imageUrl is exist but not valid
    if (body.imageUrl !== undefined && body.imageUrl.trim() === "") {
        return res.status(400).json({
            status: "Bad Request",
            message: "Image URL is invalid"
        });
    }
    // check if buyPrice is exist but not valid
    if (body.buyPrice !== undefined && (!Number.isInteger(body.buyPrice) || body.buyPrice < 0)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Buy Price is invalid"
        });
    }
    // check if promotionPrice is exist but not valid
    if (body.promotionPrice !== undefined && (!Number.isInteger(body.promotionPrice) || body.promotionPrice < 0)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Promotion Price is invalid"
        });
    }
    // check if amount is exist but not valid
    if (body.amount !== undefined && (!Number.isInteger(body.amount) || body.amount < 0)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Amount is invalid"
        });
    }
    // B3: Gọi model và xử lý CSDL
    let updateProduct = {
        name: body.name,
        description: body.description,
        type: body.type,
        imageUrl: body.imageUrl,
        buyPrice: body.buyPrice,
        promotionPrice: body.promotionPrice,
        amount: body.amount
    }
    productModel.findByIdAndUpdate(productId, updateProduct, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Server Internal Error",
                message: err.message
            });
        }
        else {
            return res.status(200).json({
                status: "Update Product successful",
                data
            });
        }
    })
}

// Delete product by ID
const deleteProduct = (req, res) => {
    // B1: Thu thập dữ liệu
    let productId = req.params.productId;
    // B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Product-Type is invalid"
        });
    };
    // B3: Gọi model và xử lý CSDL
    productModel.findByIdAndDelete(productId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Server Internal Error",
                message: err.message
            });
        }
        else {
            return res.status(204).json();
        };
    });
};

module.exports = { createProduct, getAllProducts, getProductById, updateProduct, deleteProduct, getProductByTypeId, getProductByFilterConditon }