const mongoose = require("mongoose");
const customerModel = require("../models/customer.model");

// create customer
const createCustomer = (req, res) => {
    // B1: Thu thập dữ liệu
    let body = req.body;
    // B2: Kiểm tra dữ liệu
    // check fullName
    if ( body.fullName === undefined || body.fullName.trim() === "" ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Full Name is invalid"
        });
    }
    // check phone
    if ( body.phone === undefined || body.phone.trim() === "" ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Phone is invalid"
        });
    }
    // check email
    if ( body.email === undefined || body.email.trim() === "" ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Email is invalid"
        });
    }
    // B3: Gọi model và xử lý CSDL
    let newCustomer = {
        fullName: body.fullName,
        phone: body.phone,
        email: body.email,
        address: body.address,
        city: body.city,
        country: body.country
    };
    
    customerModel.create(newCustomer, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Server Internal Error",
                message: err.message
            });
        }
        else {
            return res.status(201).json({
                status: "Create Customer successful",
                data
            });
        }
    })
}

// Get all customers
const getAllCustomers = (req, res) => {
    // B1: Thu thập dữ liệu
    // B2: Kiểm tra dữ liệu
    // B3: Gọi model và xử lý CSDL
    customerModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Server Internal Error",
                message: err.message
            });
        }
        else {
            return res.status(200).json({
                status: "Get All Customers successful",
                data
            });
        }
    })
}

// Get customer detail by ID
const getCustomerDetailById = (req, res) => {
    // B1: Thu thập dữ liệu
    let customerId = req.params.customerId;
    // B2: Kiểm tra dữ liệu
    if ( !mongoose.Types.ObjectId.isValid(customerId) ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Customer ID is invalid"
        });
    }
    // B3: Gọi model và xử lý CSDL
    customerModel.findById(customerId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Server Internal Error",
                message: err.message
            });
        }
        else {
            return res.status(200).json({
                status: "Get Customer Detail successful",
                data
            });
        }
    })
}

// Update customer
const updateCustomer = (req, res) => {
    // B1: Thu thập dữ liệu
    let customerId = req.params.customerId;
    let body = req.body;
    // B2: Kiểm tra dữ liệu
    if ( !mongoose.Types.ObjectId.isValid(customerId) ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Customer ID is invalid"
        });
    }
    // check if fullName is exist but not valid
    if ( body.fullName !== undefined && body.fullName.trim() === "" ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Full Name is invalid"
        });
    }
    // check if phone is exist but not valid
    if ( body.phone !== undefined && body.phone.trim() === "" ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Phone is invalid"
        });
    }
    // check if email is exist but not valid
    if ( body.email !== undefined && body.email.trim() === "" ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Email is invalid"
        });
    }
    // B3: Gọi model và xử lý CSDL
    let updateCustomer = {
        fullName: body.fullName,
        phone: body.phone,
        email: body.email,
        address: body.address,
        city: body.city,
        country: body.country
    };
    customerModel.findByIdAndUpdate(customerId, updateCustomer, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Server Internal Error",
                message: err.message
            });
        }
        else {
            return res.status(200).json({
                status: "Update Customer successful",
                data
            });
        }
    })
}

// create customer
const deleteCustomer = (req, res) => {
    // B1: Thu thập dữ liệu
    let customerId = req.params.customerId;
    // B2: Kiểm tra dữ liệu
    if ( !mongoose.Types.ObjectId.isValid(customerId) ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Customer ID is invalid"
        });
    }
    // B3: Gọi model và xử lý CSDL
    customerModel.findByIdAndDelete(customerId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Server Internal Error",
                message: err.message
            });
        }
        else {
            return res.status(204).json();
        }
    })
}

module.exports = { createCustomer, getAllCustomers, getCustomerDetailById, updateCustomer, deleteCustomer }