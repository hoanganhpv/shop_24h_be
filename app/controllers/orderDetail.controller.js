const mongoose = require("mongoose");
const orderModel = require("../models/order.model");
const orderDetailModel = require("../models/orderDetail.model");

// Create order detail of an order
const createAnOrderDetailOfOrder = (req, res) => {
    // B1: Thu thập dữ liệu
    let orderId = req.params.orderId;
    let body = req.body;
    // B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Order ID is invalid"
        });
    }
    if (!mongoose.Types.ObjectId.isValid(body.product)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Product is invalid"
        });
    }
    if (!Number.isInteger(body.quantity) || body.quantity < 0) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Quantity is invalid"
        });
    }
    // B3: Gọi model và xử lý CSDL
    let newOrderDetail = {
        product: body.product,
        quantity: body.quantity
    }
    orderDetailModel.create(newOrderDetail, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Server Internal Error",
                message: err.message
            });
        }
        else {
            orderModel.findByIdAndUpdate(orderId, {
                $push: {orderDetails: data._id}
            }, (err1, data1) => {
                if (err1) {
                    return res.status(500).json({
                        status: "Server Internal Error",
                        message: err1.message
                    });
                }
            })
            return res.status(201).json({
                status: "Create Order Detail successful",
                data
            });
        }
    })
}

// Get all order detail of an order
const getAllOrderDetailOfAnOrder = (req, res) => {
    // B1: Thu thập dữ liệu
    let orderId = req.params.orderId;
    // B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Order ID is invalid"
        });
    }
    // B3: Gọi model và xử lý CSDL
    orderModel.findById(orderId).populate("orderDetails").exec((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Server Internal Error",
                message: err.message
            });
        }
        else {
            return res.status(200).json({
                status: "Get All Order Details of an Order successful",
                data: data.orderDetails
            })
        }
    })
}

// Get order detail by ID
const getAnOrderDetailById = (req, res) => {
    // B1: Thu thập dữ liệu
    let orderDetailId = req.params.detailId;
    // B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Order-Detail ID is invalid"
        });
    }
    // B3: Gọi model và xử lý CSDL
    orderDetailModel.findById(orderDetailId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Server Internal Error",
                message: err.message
            });
        }
        else {
            return res.status(200).json({
                status: "Get Order-Details successful",
                data
            })
        }
    })
}

// Update order detail by ID
const updateOrderDetail = (req, res) => {
    // B1: Thu thập dữ liệu
    let orderDetailId = req.params.detailId;
    let body = req.body
    // B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Order-Detail ID is invalid"
        });
    }
    // check product objectId
    if (body.product !== undefined && !mongoose.Types.ObjectId.isValid(body.product)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Product is invalid"
        });
    }
    // check quantity
    if ( body.quantity !== undefined && (!Number.isInteger(body.quantity) || body.quantity < 0) ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Quantity is invalid"
        });
    }
    // B3: Gọi model và xử lý CSDL
    let updateOrderDetail = {
        product: body.product,
        quantity: body.quantity
    }
    orderDetailModel.findByIdAndUpdate(orderDetailId, updateOrderDetail, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Server Internal Error",
                message: err.message
            });
        }
        else {
            return res.status(200).json({
                status: "Update Order-Details successful",
                data
            })
        }
    })
}

// Delete order detail by ID
const deleteOrderDetail = (req, res) => {
    // B1: Thu thập dữ liệu
    let orderDetailId = req.params.detailId;
    let orderId = req.params.orderId;
    // B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Order-Detail ID is invalid"
        });
    }
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Order ID is invalid"
        });
    }
    // B3: Gọi model và xử lý CSDL
    orderDetailModel.findByIdAndDelete(orderDetailId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Server Internal Error",
                message: err.message
            });
        }
        else {
            orderModel.findByIdAndUpdate(orderId, {
                $pull: {orderDetails: orderDetailId}
            }, (err1, data1) => {
                if (err1) {
                    return res.status(500).json({
                        status: "Server Internal Error",
                        message: err1.message
                    });
                }
            })
            return res.status(204).json();
        }
    })
}

module.exports = { createAnOrderDetailOfOrder, getAllOrderDetailOfAnOrder, getAnOrderDetailById, updateOrderDetail, deleteOrderDetail }